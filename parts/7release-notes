#!/bin/sh -e

. `dirname $0`/../common.sh

notesdir=$webtopdir/release-notes

echo "Running the full release notes build... (at `date`)"

savelog -g debwww -m 664 $notesdir/build.log >/dev/null

date > $notesdir/build.log

for release in jessie stretch ; do
    echo "rebuilding the release notes for $release" >> $notesdir/build.log
    cd $notesdir
    if ! [ -d $notesdir/release-notes/.git ]; then
        echo "Directory $notesdir/release-notes is not a git repository. Cloning from git." >> $notesdir/build.log
        git clone https://salsa.debian.org/ddp-team/release-notes.git >> $notesdir/build.log 2>&1
    fi
    cd $notesdir/release-notes
    if [ "$release" = "stretch" ]; then
        # Stay on master
        git checkout master >> $notesdir/build.log 2>&1 && git pull >> $notesdir/build.log 2>&1
    else
        # Switch to $release branch
        git checkout $release >> $notesdir/build.log 2>&1 && git pull >> $notesdir/build.log 2>&1
    fi
    make -C $notesdir/release-notes publish \
        PUBLISHTARBALL=yes PUBLISHDIR=$webtopdir/www/releases/$release >> $notesdir/build.log 2>&1
done

date >> $notesdir/build.log

echo "Full release notes build finished (at `date`)"

echo
