#! /bin/sh

umask 002

excl="/doc/manuals /releases /doc/developers-reference /doc/maint-guide /doc/FAQ /searchtmpl /ports/powerpc/inst/yaboot-howto"
# excluding ro (workaround for #716848)
excl_files="/international/l10n/po/ro"

wwwroot=/srv/www.debian.org/www
dirout=/srv/www-master.debian.org/htdocs/build-logs/tidy

progname=`echo $0 | sed 's/.*\///'`

PATH=$HOME/extra/bin:$PATH
export PATH

opt_v=false
opt_h=false
opt_t=false
opt_q=true
for opt
do
    case $opt in
    -h | --help )
        shift
        opt_h=true ;;
    -v | --verbose )
        shift
        opt_v=true ;;
    -d | --debug )
        shift
        set -x ;;
    -t | --ignore-trim )
        shift
        opt_t=true ;;
    -q | --nested-q )
	shift
	opt_q=false ;;
    -* )
        { echo "Error: unknown option
Try $progname --help'" 1>&2; exit 1; } ;;
    * )
        break ;;
    esac
done

if $opt_h; then
    cat <<EOT
Usage: $progname [options] file ...
Options:
  -h --help            display this help
  -v  --verbose        run verbose
  -t  --ignore-trim    remove tidy messages about 'trimming'
  -q  --nested-q       don't ignore tidy messages about nested q elements
EOT
    exit 0
fi

#  Lang list
if test $# = 0; then
    set x `ls $wwwroot/index.*.html | sed -e 's/^.*index\.//' -e 's/\.html//'`
    shift
fi

#  Exclude paths
www_bs=`echo $wwwroot | sed -e 's/\//\\\\\//g'`
excl_path=`echo $excl | sed -e "s/ / -o -path $www_bs/g"`

test -d $dirout || mkdir -p $dirout

if $opt_t; then
    excludetrim='trimming'
elif $opt_q; then
    excludetrim='nested q elements'
else
    excludetrim='NoWayThatThisStringReallyMatch'
fi

for l
do
    tidyopts=
    case $l in
      zh-hk ) continue ;; # only process zh-cn and zh-tw
    esac
    test -L $wwwroot/index.$l.html && continue
    $opt_v && echo Processing language $l
    excl_name=''
    for f in $excl_files
    do
	excl_name="$excl_name -o -wholename $wwwroot"`echo $f | sed -e "s/ /.$l.html -o -wholename $www_bs/g"`".$l.html"
    done
    for i in `find $wwwroot \( -path $wwwroot$excl_path $excl_name \) \
		-prune -o -name \*.$l.html -print | sort`;
    do
        echo \*\*\* $i
	enc=`grep -i '<meta http-equiv="Content-Type"' $i | sed -e 's/.*charset=//' -e 's/".*//'`
	if iconv -f $enc -t utf-8 $i >/dev/null 2>&1;
	then
	    iconv -f $enc -t utf-8 $i | tidy -eq -utf8 - 2>&1 | grep ^line | grep -v "$excludetrim"
	else
	    echo $i 1>&2
	fi
    done | perl -e 'while (<>) {unless (m/^line/) {$last=$_; next;} if ($last) {print $last; $last="";} print;}' > $dirout/$l
done
